# demo-batch-spring5

## Environnement technique
* JDK 8
* Spring 5
* Spring Boot 2
* Spring Data (LdapTemplate, JdbcTemplate)


## Spécifications fonctionnelles détaillées
Les principales étapes du traitement batch sont les suivantes :
	
* Etape 1 : en sortie, l’ensemble ES des utilisateurs actifs, désactivés, ou inactifs en base de données 
* Etape 2 : en entrée,  l’ensemble ES des utilisateurs actifs, désactivés, ou inactifs en base de données

Récupérer l’ensemble EL des utilisateurs du LDAP

En sortie, les élément de la différence ES-EL

* Etape 3 : Pour chaque élément de la différence ES-EL
- Si l’utilisateur est actif, le supprimer de la table 
- Si l’utilisateur est désactivé depuis plus de 183 jour, le supprimer de la table  

* Etape 4 : Supprimer de la table des utilisateurs restants, ceux qui satisfonts les conditions supplémentaires



## Documentation
https://jeremy-jeanne.developpez.com/tutoriels/spring/spring-batch/

https://docs.spring.io/spring-ldap/docs/current/reference/

https://stackoverflow.com/questions/53102998/spring-boot-1-5-9-application-not-working-in-java-1-7

https://stackoverflow.com/questions/53018034/how-to-autowire-ldaprepository-in-spring