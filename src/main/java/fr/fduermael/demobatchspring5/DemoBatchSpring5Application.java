package fr.fduermael.demobatchspring5;

import fr.fduermael.demobatchspring5.step.Step1JpaUsers;
import fr.fduermael.demobatchspring5.step.Step2MinusLdapUsers;
import fr.fduermael.demobatchspring5.step.Step3DeleteActiveUsers;
import fr.fduermael.demobatchspring5.step.Step4DeleteOtherUsers;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoBatchSpring5Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoBatchSpring5Application.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {

			Step1JpaUsers step1 = new Step1JpaUsers(ctx);
			step1.process();

			Step2MinusLdapUsers step2 = new Step2MinusLdapUsers(ctx);
			step2.setInput(step1.getOutput());
			step2.process();

			Step3DeleteActiveUsers step3 = new Step3DeleteActiveUsers(ctx);
			step3.setInput(step2.getOutput());
			step3.process();

			Step4DeleteOtherUsers step4 = new Step4DeleteOtherUsers(ctx);
			step4.setInput(step3.getOutput());
			step4.process();


		};
	}

}
