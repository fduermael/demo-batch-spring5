package fr.fduermael.demobatchspring5.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "expandocolumn")
public class Expandocolumn implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "columnid", insertable = false, nullable = false)
    private Long columnid;

    @Column(name = "companyid")
    private Long companyid;

    @Column(name = "tableid")
    private Long tableid;

    @Column(name = "name")
    private String name;

    @Column(name = "type_")
    private Integer type;

    @Column(name = "defaultdata")
    private String defaultdata;

    @Column(name = "typesettings")
    private String typesettings;

    public Long getColumnid() {
        return columnid;
    }

    public void setColumnid(Long columnid) {
        this.columnid = columnid;
    }

    public Long getCompanyid() {
        return companyid;
    }

    public void setCompanyid(Long companyid) {
        this.companyid = companyid;
    }

    public Long getTableid() {
        return tableid;
    }

    public void setTableid(Long tableid) {
        this.tableid = tableid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDefaultdata() {
        return defaultdata;
    }

    public void setDefaultdata(String defaultdata) {
        this.defaultdata = defaultdata;
    }

    public String getTypesettings() {
        return typesettings;
    }

    public void setTypesettings(String typesettings) {
        this.typesettings = typesettings;
    }

    public String toString() {
      return "Expandocolumn{columnid=" + columnid + 
        ", companyid=" + companyid + 
        ", tableid=" + tableid + 
        ", name=" + name + 
        ", type=" + type + 
        ", defaultdata=" + defaultdata + 
        ", typesettings=" + typesettings + 
        "}";
    }
}