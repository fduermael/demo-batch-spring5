package fr.fduermael.demobatchspring5.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "expandovalue")
public class Expandovalue implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "valueid", insertable = false, nullable = false)
    private Long valueid;

    @Column(name = "companyid")
    private Long companyid;

    @Column(name = "tableid")
    private Long tableid;

    @Column(name = "columnid")
    private Long columnid;

    @Column(name = "rowid_")
    private Long rowid;

    @Column(name = "classnameid")
    private Long classnameid;

    @Column(name = "classpk")
    private Long classpk;

    @Column(name = "data_")
    private String data;

    public Long getValueid() {
        return valueid;
    }

    public void setValueid(Long valueid) {
        this.valueid = valueid;
    }

    public Long getCompanyid() {
        return companyid;
    }

    public void setCompanyid(Long companyid) {
        this.companyid = companyid;
    }

    public Long getTableid() {
        return tableid;
    }

    public void setTableid(Long tableid) {
        this.tableid = tableid;
    }

    public Long getColumnid() {
        return columnid;
    }

    public void setColumnid(Long columnid) {
        this.columnid = columnid;
    }

    public Long getRowid() {
        return rowid;
    }

    public void setRowid(Long rowid) {
        this.rowid = rowid;
    }

    public Long getClassnameid() {
        return classnameid;
    }

    public void setClassnameid(Long classnameid) {
        this.classnameid = classnameid;
    }

    public Long getClasspk() {
        return classpk;
    }

    public void setClasspk(Long classpk) {
        this.classpk = classpk;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String toString() {
      return "Expandovalue{valueid=" + valueid + 
        ", companyid=" + companyid + 
        ", tableid=" + tableid + 
        ", columnid=" + columnid + 
        ", rowid=" + rowid + 
        ", classnameid=" + classnameid + 
        ", classpk=" + classpk + 
        ", data=" + data + 
        "}";
    }
}