package fr.fduermael.demobatchspring5.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "expandorow")
public class Expandorow implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "rowid_", insertable = false, nullable = false)
    private Long rowid;

    @Column(name = "companyid")
    private Long companyid;

    @Column(name = "tableid")
    private Long tableid;

    @Column(name = "classpk")
    private Long classpk;

    public Long getRowid() {
        return rowid;
    }

    public void setRowid(Long rowid) {
        this.rowid = rowid;
    }

    public Long getCompanyid() {
        return companyid;
    }

    public void setCompanyid(Long companyid) {
        this.companyid = companyid;
    }

    public Long getTableid() {
        return tableid;
    }

    public void setTableid(Long tableid) {
        this.tableid = tableid;
    }

    public Long getClasspk() {
        return classpk;
    }

    public void setClasspk(Long classpk) {
        this.classpk = classpk;
    }

    public String toString() {
      return "Expandorow{rowid=" + rowid + 
        ", companyid=" + companyid + 
        ", tableid=" + tableid + 
        ", classpk=" + classpk + 
        "}";
    }
}