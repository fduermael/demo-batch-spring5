package fr.fduermael.demobatchspring5.entity;

import org.hibernate.annotations.Formula;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.*;

@Entity
@Table(name = "user_")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "uuid_")
    private String uuid;

    /*
    @Formula("(select v.data_ from Expandocolumn c, Expandovalue v where  c.name='MCCIsGuest' and c.columnid=v.columnid and v.classpk=userid)")
    private String mccGuest;

    @Formula("(select v.data_ from Expandocolumn c, Expandovalue v where  c.name='MCCIsExternalMember' and c.columnid=v.columnid and v.classpk=userid)")
    private String mccExternalMember;


     */
    @Id
    @Column(name = "userid", insertable = false, nullable = false)
    private Long userid;

    @Column(name = "companyid")
    private Long companyid;

    @Column(name = "createdate")
    private Timestamp createdate;

    @Column(name = "modifieddate")
    private Timestamp modifieddate;

    @Column(name = "defaultuser")
    private Boolean defaultuser;

    @Column(name = "contactid")
    private Long contactid;

    @Column(name = "password_")
    private String password;

    @Column(name = "passwordencrypted")
    private Boolean passwordencrypted;

    @Column(name = "passwordreset")
    private Boolean passwordreset;

    @Column(name = "passwordmodifieddate")
    private Timestamp passwordmodifieddate;

    @Column(name = "digest")
    private String digest;

    @Column(name = "reminderqueryquestion")
    private String reminderqueryquestion;

    @Column(name = "reminderqueryanswer")
    private String reminderqueryanswer;

    @Column(name = "gracelogincount")
    private Integer gracelogincount;

    @Column(name = "screenname")
    private String screenname;

    @Column(name = "emailaddress")
    private String emailaddress;

    @Column(name = "facebookid")
    private Long facebookid;

    @Column(name = "openid")
    private String openid;

    @Column(name = "portraitid")
    private Long portraitid;

    @Column(name = "languageid")
    private String languageid;

    @Column(name = "timezoneid")
    private String timezoneid;

    @Column(name = "greeting")
    private String greeting;

    @Column(name = "comments")
    private String comments;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "middlename")
    private String middlename;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "jobtitle")
    private String jobtitle;

    @Column(name = "logindate")
    private Timestamp logindate;

    @Column(name = "loginip")
    private String loginip;

    @Column(name = "lastlogindate")
    private Timestamp lastlogindate;

    @Column(name = "lastloginip")
    private String lastloginip;

    @Column(name = "lastfailedlogindate")
    private Timestamp lastfailedlogindate;

    @Column(name = "failedloginattempts")
    private Integer failedloginattempts;

    @Column(name = "lockout")
    private Boolean lockout;

    @Column(name = "lockoutdate")
    private Timestamp lockoutdate;

    @Column(name = "agreedtotermsofuse")
    private Boolean agreedtotermsofuse;

    @Column(name = "emailaddressverified")
    private Boolean emailaddressverified;

    @Column(name = "status")
    private Integer status;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public Long getCompanyid() {
        return companyid;
    }

    public void setCompanyid(Long companyid) {
        this.companyid = companyid;
    }

    public Timestamp getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Timestamp createdate) {
        this.createdate = createdate;
    }

    public Timestamp getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(Timestamp modifieddate) {
        this.modifieddate = modifieddate;
    }

    public Boolean isDefaultuser() {
        return defaultuser;
    }

    public void setDefaultuser(Boolean defaultuser) {
        this.defaultuser = defaultuser;
    }

    public Long getContactid() {
        return contactid;
    }

    public void setContactid(Long contactid) {
        this.contactid = contactid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean isPasswordencrypted() {
        return passwordencrypted;
    }

    public void setPasswordencrypted(Boolean passwordencrypted) {
        this.passwordencrypted = passwordencrypted;
    }

    public Boolean isPasswordreset() {
        return passwordreset;
    }

    public void setPasswordreset(Boolean passwordreset) {
        this.passwordreset = passwordreset;
    }

    public Timestamp getPasswordmodifieddate() {
        return passwordmodifieddate;
    }

    public void setPasswordmodifieddate(Timestamp passwordmodifieddate) {
        this.passwordmodifieddate = passwordmodifieddate;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getReminderqueryquestion() {
        return reminderqueryquestion;
    }

    public void setReminderqueryquestion(String reminderqueryquestion) {
        this.reminderqueryquestion = reminderqueryquestion;
    }

    public String getReminderqueryanswer() {
        return reminderqueryanswer;
    }

    public void setReminderqueryanswer(String reminderqueryanswer) {
        this.reminderqueryanswer = reminderqueryanswer;
    }

    public Integer getGracelogincount() {
        return gracelogincount;
    }

    public void setGracelogincount(Integer gracelogincount) {
        this.gracelogincount = gracelogincount;
    }

    public String getScreenname() {
        return screenname;
    }

    public void setScreenname(String screenname) {
        this.screenname = screenname;
    }

    public String getEmailaddress() {
        return emailaddress;
    }

    public void setEmailaddress(String emailaddress) {
        this.emailaddress = emailaddress;
    }

    public Long getFacebookid() {
        return facebookid;
    }

    public void setFacebookid(Long facebookid) {
        this.facebookid = facebookid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Long getPortraitid() {
        return portraitid;
    }

    public void setPortraitid(Long portraitid) {
        this.portraitid = portraitid;
    }

    public String getLanguageid() {
        return languageid;
    }

    public void setLanguageid(String languageid) {
        this.languageid = languageid;
    }

    public String getTimezoneid() {
        return timezoneid;
    }

    public void setTimezoneid(String timezoneid) {
        this.timezoneid = timezoneid;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getJobtitle() {
        return jobtitle;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }

    public Timestamp getLogindate() {
        return logindate;
    }

    public void setLogindate(Timestamp logindate) {
        this.logindate = logindate;
    }

    public String getLoginip() {
        return loginip;
    }

    public void setLoginip(String loginip) {
        this.loginip = loginip;
    }

    public Timestamp getLastlogindate() {
        return lastlogindate;
    }

    public void setLastlogindate(Timestamp lastlogindate) {
        this.lastlogindate = lastlogindate;
    }

    public String getLastloginip() {
        return lastloginip;
    }

    public void setLastloginip(String lastloginip) {
        this.lastloginip = lastloginip;
    }

    public Timestamp getLastfailedlogindate() {
        return lastfailedlogindate;
    }

    public void setLastfailedlogindate(Timestamp lastfailedlogindate) {
        this.lastfailedlogindate = lastfailedlogindate;
    }

    public Integer getFailedloginattempts() {
        return failedloginattempts;
    }

    public void setFailedloginattempts(Integer failedloginattempts) {
        this.failedloginattempts = failedloginattempts;
    }

    public Boolean isLockout() {
        return lockout;
    }

    public void setLockout(Boolean lockout) {
        this.lockout = lockout;
    }

    public Timestamp getLockoutdate() {
        return lockoutdate;
    }

    public void setLockoutdate(Timestamp lockoutdate) {
        this.lockoutdate = lockoutdate;
    }

    public Boolean isAgreedtotermsofuse() {
        return agreedtotermsofuse;
    }

    public void setAgreedtotermsofuse(Boolean agreedtotermsofuse) {
        this.agreedtotermsofuse = agreedtotermsofuse;
    }

    public Boolean isEmailaddressverified() {
        return emailaddressverified;
    }

    public void setEmailaddressverified(Boolean emailaddressverified) {
        this.emailaddressverified = emailaddressverified;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    /*
    public boolean isMccGuest() {
        return "true".equals(mccGuest);
    }

    public boolean isMccExternalMember() {
        return "true".equals(mccExternalMember);
    }
    */


    public String toString() {
      return "User{uuid=" + uuid + 
        ", userid=" + userid + 
        ", companyid=" + companyid + 
        ", createdate=" + createdate + 
        ", modifieddate=" + modifieddate + 
        ", defaultuser=" + defaultuser + 
        ", contactid=" + contactid + 
        ", password=" + password + 
        ", passwordencrypted=" + passwordencrypted + 
        ", passwordreset=" + passwordreset + 
        ", passwordmodifieddate=" + passwordmodifieddate + 
        ", digest=" + digest + 
        ", reminderqueryquestion=" + reminderqueryquestion + 
        ", reminderqueryanswer=" + reminderqueryanswer + 
        ", gracelogincount=" + gracelogincount + 
        ", screenname=" + screenname + 
        ", emailaddress=" + emailaddress + 
        ", facebookid=" + facebookid + 
        ", openid=" + openid + 
        ", portraitid=" + portraitid + 
        ", languageid=" + languageid + 
        ", timezoneid=" + timezoneid + 
        ", greeting=" + greeting + 
        ", comments=" + comments + 
        ", firstname=" + firstname + 
        ", middlename=" + middlename + 
        ", lastname=" + lastname + 
        ", jobtitle=" + jobtitle + 
        ", logindate=" + logindate + 
        ", loginip=" + loginip + 
        ", lastlogindate=" + lastlogindate + 
        ", lastloginip=" + lastloginip + 
        ", lastfailedlogindate=" + lastfailedlogindate + 
        ", failedloginattempts=" + failedloginattempts + 
        ", lockout=" + lockout + 
        ", lockoutdate=" + lockoutdate + 
        ", agreedtotermsofuse=" + agreedtotermsofuse + 
        ", emailaddressverified=" + emailaddressverified + 
        ", status=" + status + 
        "}";
    }
}