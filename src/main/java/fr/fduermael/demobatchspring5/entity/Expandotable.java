package fr.fduermael.demobatchspring5.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "expandotable")
public class Expandotable implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "tableid", insertable = false, nullable = false)
    private Long tableid;

    @Column(name = "companyid")
    private Long companyid;

    @Column(name = "classnameid")
    private Long classnameid;

    @Column(name = "name")
    private String name;

    public Long getTableid() {
        return tableid;
    }

    public void setTableid(Long tableid) {
        this.tableid = tableid;
    }

    public Long getCompanyid() {
        return companyid;
    }

    public void setCompanyid(Long companyid) {
        this.companyid = companyid;
    }

    public Long getClassnameid() {
        return classnameid;
    }

    public void setClassnameid(Long classnameid) {
        this.classnameid = classnameid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
      return "Expandotable{tableid=" + tableid + 
        ", companyid=" + companyid + 
        ", classnameid=" + classnameid + 
        ", name=" + name + 
        "}";
    }
}