package fr.fduermael.demobatchspring5.repository;

import fr.fduermael.demobatchspring5.entity.Expandotable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ExpandotableRepository extends JpaRepository<Expandotable, Long>, JpaSpecificationExecutor<Expandotable> {

}