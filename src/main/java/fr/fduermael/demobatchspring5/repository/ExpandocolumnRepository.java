package fr.fduermael.demobatchspring5.repository;

import fr.fduermael.demobatchspring5.entity.Expandocolumn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ExpandocolumnRepository extends JpaRepository<Expandocolumn, Long>, JpaSpecificationExecutor<Expandocolumn> {

}