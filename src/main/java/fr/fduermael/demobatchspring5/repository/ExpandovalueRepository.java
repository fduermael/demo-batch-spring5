package fr.fduermael.demobatchspring5.repository;

import fr.fduermael.demobatchspring5.entity.Expandovalue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ExpandovalueRepository extends JpaRepository<Expandovalue, Long>, JpaSpecificationExecutor<Expandovalue> {

}