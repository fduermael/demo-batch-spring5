package fr.fduermael.demobatchspring5.repository;

import fr.fduermael.demobatchspring5.entity.Expandorow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ExpandorowRepository extends JpaRepository<Expandorow, Long>, JpaSpecificationExecutor<Expandorow> {

}