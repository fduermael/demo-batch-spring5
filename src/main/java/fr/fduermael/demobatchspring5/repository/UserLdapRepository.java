package fr.fduermael.demobatchspring5.repository;

import org.springframework.data.ldap.repository.LdapRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserLdapRepository extends LdapRepository<User>
{
    List<String> getAllUserNames();

    List<String> getAllUserId();
}
