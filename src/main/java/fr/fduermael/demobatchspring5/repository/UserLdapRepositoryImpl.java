package fr.fduermael.demobatchspring5.repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.NamingException;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

import javax.naming.Name;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import java.util.List;
import java.util.Optional;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

@Repository
public class UserLdapRepositoryImpl implements UserLdapRepository {

    @Autowired
    private LdapTemplate ldapTemplate;

    public void setLdapTemplate(LdapTemplate ldapTemplate) {
        this.ldapTemplate = ldapTemplate;
    }

    public List<String> getAllUserNames() {
        return ldapTemplate.search(
                query().where("objectclass").is("person"),
                new AttributesMapper<String>() {
                    public String mapFromAttributes(Attributes attrs)
                            throws NamingException, javax.naming.NamingException {
                        return attrs.get("cn").get().toString().toUpperCase();
                    }
                });
    }

    public List<String> getAllUserId() {
        return ldapTemplate.search(
                query().where("objectclass").is("person"),
                new AttributesMapper<String>() {
                    public String mapFromAttributes(Attributes attrs)
                            throws NamingException, javax.naming.NamingException {
                        Attribute uidAttr = attrs.get("uid");

                        if (uidAttr != null) {
                            return uidAttr.get().toString();
                        }
                        return "";
                    }
                });
    }

    @Override
    public Optional<User> findOne(LdapQuery ldapQuery) {
        return Optional.empty();
    }

    @Override
    public Iterable<User> findAll(LdapQuery ldapQuery) {
        return null;
    }

    @Override
    public <S extends User> S save(S s) {
        return null;
    }

    @Override
    public <S extends User> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<User> findById(Name name) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Name name) {
        return false;
    }

    @Override
    public Iterable<User> findAll() {
        return null;
    }

    @Override
    public Iterable<User> findAllById(Iterable<Name> iterable) {
        return null;
    }

    @Override
    public long count() {
        return getAllUserNames().size();
    }

    @Override
    public void deleteById(Name name) {

    }

    @Override
    public void delete(User user) {

    }

    @Override
    public void deleteAll(Iterable<? extends User> iterable) {

    }

    @Override
    public void deleteAll() {

    }
}