package fr.fduermael.demobatchspring5;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.ldap.repository.config.EnableLdapRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import javax.sql.DataSource;

@Configuration
@EnableAutoConfiguration
@ComponentScan
//@ConfigurationProperties
@EnableLdapRepositories
@EnableJpaRepositories
//https://mkyong.com/spring/spring-propertysources-example/
@PropertySources({
@PropertySource("classpath:application-integ.properties"),
@PropertySource(value="file:${semaphore.rgpd.batch.properties.file}", ignoreResourceNotFound=true)}
)
//@ImportResource({"classpath*:applicationContext.xml"})
public class Config {

    @Value("${spring.ldap.url}")
    String ldapUrl;

    @Value("${spring.ldap.base}")
    String ldapBase;

    @Value("${spring.ldap.username}")
    String ldapUsername;

    @Value("${spring.ldap.password}")
    String ldapPassword;

    @Value("${spring.datasource.url}")
    String jdbcUrl;

    @Value("${spring.datasource.driverclassname}")
    String jdbcDriverclassname;

    @Value("${spring.datasource.username}")
    String jdbcUsername;

    @Value("${spring.datasource.password}")
    String jdbcPassword;

    @Bean
    public LdapContextSource contextSource () {
        LdapContextSource contextSource= new LdapContextSource();
        contextSource.setUrl(ldapUrl);
        contextSource.setBase(ldapBase);
        contextSource.setUserDn(ldapUsername);
        contextSource.setPassword(ldapPassword);
        return contextSource;
    }

    @Bean
    public LdapTemplate ldapTemplate() {
        return new LdapTemplate(contextSource());
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(jdbcDriverclassname);
        dataSource.setUrl(jdbcUrl);
        dataSource.setUsername(jdbcUsername);
        dataSource.setPassword(jdbcPassword);

        return dataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
        return propertySourcesPlaceholderConfigurer;
    }

}