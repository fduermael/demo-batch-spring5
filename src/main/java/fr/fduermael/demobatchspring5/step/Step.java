package fr.fduermael.demobatchspring5.step;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

public abstract class Step<T_INPUT,T_OUTPUT> {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    protected T_INPUT input;

    protected T_OUTPUT output;

    protected ApplicationContext applicationContext;

    public Step(ApplicationContext ctx) {
        applicationContext = ctx;
    }

    public T_INPUT getInput() {
        return input;
    }

    public void setInput(T_INPUT input) {
        this.input = input;
    }

    public T_OUTPUT getOutput() {
        return output;
    }

    public void setOutput(T_OUTPUT output) {
        this.output = output;
    }


    abstract public void process();

    public boolean isSimulation() {
        boolean b = Boolean.parseBoolean( System.getProperty( "simulation" ) );
        return  b;
    }

}
