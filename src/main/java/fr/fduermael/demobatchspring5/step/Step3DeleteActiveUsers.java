package fr.fduermael.demobatchspring5.step;

import fr.fduermael.demobatchspring5.Config;
import fr.fduermael.demobatchspring5.entity.User;
import fr.fduermael.demobatchspring5.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * input : utilisateurs de la base qui ne figurent pas dans l'annuaire
 * output : utilisateurs sur lesquels on doit encore vérifier les règles de gestion avant de les supprimer
 */

@Component
public class Step3DeleteActiveUsers extends Step<List<User>, List<User>>{
    public Step3DeleteActiveUsers(ApplicationContext ctx) {
        super(ctx);
    }

    @Override
    public void process() {
        logger.info("Step 3");
        UserRepository userRepository = (UserRepository) applicationContext.getBean("userRepository");

        output = new ArrayList<User>();
        for (User user: input) {
            if (user.getLastlogindate()!= null && user.getLastlogindate().after(Timestamp.from(Instant.now().minusSeconds(3600*24*183)))) {
               logger.warn("Utilisateur " +user.getLastname()+" "+user.getFirstname()+" encore actif devant être supprimé");
                if (!isSimulation()) {
                    logger.warn("Supprimé");
                    //userRepository.deleteById(user.getUserid());
                } else {
                    logger.warn("Suppression simulée");

                }
            } else {
                output.add(user);
            }
        }
        logger.info(output.size()+" utilisateurs sur lesquels on doit encore vérifier les règles de gestion avant de les supprimer.");
    }
}
