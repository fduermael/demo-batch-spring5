package fr.fduermael.demobatchspring5.step;

import fr.fduermael.demobatchspring5.entity.User;
import fr.fduermael.demobatchspring5.repository.UserRepository;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * input : utilisateurs sur lesquels on doit encore vérifier les règles de gestion avant de les supprimer
 * output : utilisateurs de la base qui n'ont pas été supprimés après application des règles de gestion
 */

public class Step4DeleteOtherUsers extends Step<List<User>, List<User>>{
    public Step4DeleteOtherUsers(ApplicationContext ctx) {
        super(ctx);
    }

    @Override
    public void process() {
        logger.info("Step 4");
        UserRepository userRepository = (UserRepository) applicationContext.getBean("userRepository");

        output = new ArrayList<User>();
        for (User user: input) {
            if (user != null) {
               // critère règle de gestion pour être supprimé
               //TODO: décommenter
               //userRepository.deleteById(user.getUserid());
            } else {
                output.add(user);
            }
        }
        logger.info(output.size()+" utilisateurs utilisateurs de la base qui n'ont pas été supprimés après application des règles de gestion");
    }
}
