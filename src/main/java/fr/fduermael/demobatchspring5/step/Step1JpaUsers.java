package fr.fduermael.demobatchspring5.step;

import fr.fduermael.demobatchspring5.entity.User;
import fr.fduermael.demobatchspring5.repository.UserRepository;
import org.springframework.context.ApplicationContext;

import java.util.List;

public class Step1JpaUsers extends Step<Void, List<User>>{
    public Step1JpaUsers(ApplicationContext ctx) {
        super(ctx);
    }

    @Override
    public void process() {
        logger.info("Step 1");
        UserRepository userRepository = (UserRepository) applicationContext.getBean("userRepository");
        logger.info(userRepository.count()+" utilisateurs en base");
        setOutput(userRepository.findAll());
    }
}
