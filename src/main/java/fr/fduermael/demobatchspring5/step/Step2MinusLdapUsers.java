package fr.fduermael.demobatchspring5.step;

import fr.fduermael.demobatchspring5.entity.User;
import fr.fduermael.demobatchspring5.repository.UserLdapRepository;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * input : utilisateurs de la base
 * output : utilisateurs de la base qui ne figurent pas dans l'annuaire
 */

public class Step2MinusLdapUsers extends Step<List<User>, List<User>>{
    public Step2MinusLdapUsers(ApplicationContext ctx) {
        super(ctx);
    }

    @Override
    public void process() {
        logger.info("Step 2");
        UserLdapRepository userLdapRepository = (UserLdapRepository) applicationContext.getBean("userLdapRepository");
        List<String> ldapUserId = userLdapRepository.getAllUserId();
        logger.info(ldapUserId.size()+" utilisateurs sur l'annuaire");
        //List<String> ldapUserNames = userLdapRepository.getAllUserNames();
        //System.out.println(ldapUserNames.size()+" utilisateurs sur l'annuaire");

        output = new ArrayList<User>();
        for (User user: input) {
            String username = user.getLastname()+ " "+user.getFirstname();
            if (user.getMiddlename()!=null && !user.getMiddlename().trim().isEmpty()) {
                username = username +" "+user.getMiddlename();

            }
            username = username.toUpperCase();

            if (!ldapUserId.contains(user.getScreenname())) {
            //if (!ldapUserNames.contains(username)) {
                output.add(user);
                logger.debug("cn:"+username);
                logger.debug(",uid:"+user.getScreenname());
            }
        }
        logger.info(output.size()+" utilisateurs qui ne figurent pas dans l'annuaire");
    }
}
