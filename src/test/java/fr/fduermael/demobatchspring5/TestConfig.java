package fr.fduermael.demobatchspring5;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.ldap.repository.config.EnableLdapRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.test.EmbeddedLdapServerFactoryBean;
import org.springframework.ldap.test.LdifPopulator;
import org.springframework.ldap.test.TestContextSourceFactoryBean;

import javax.annotation.Resource;
import javax.sql.DataSource;

@Configuration
//@ConfigurationProperties
//@EnableLdapRepositories
//@EnableJpaRepositories
//https://mkyong.com/spring/spring-propertysources-example/
//@PropertySources({
//@PropertySource("classpath:application-test.properties")
//})
@ImportResource({"classpath*:applicationContext-test.xml"})
public class TestConfig {

    @Value("${spring.ldap.embedded.port}")
    String ldapPort;

    @Value("${spring.ldap.embedded.base-dn}")
    String ldapBase;

    @Value("${spring.ldap.embedded.credential.username}")
    String ldapUsername;

    @Value("${spring.ldap.embedded.credential.password}")
    String ldapPassword;



//    @Value("${spring.datasource.url}")
//    String jdbcUrl;
//
//    @Value("${spring.datasource.driverclassname}")
//    String jdbcDriverclassname;
//
//    @Value("${spring.datasource.username}")
//    String jdbcUsername;
//
//    @Value("${spring.datasource.password}")
//    String jdbcPassword;
//

//    @Bean
//    public EmbeddedLdapServerFactoryBean embeddedLdapServer () {
//        EmbeddedLdapServerFactoryBean embeddedLdapServer= new EmbeddedLdapServerFactoryBean();
//        embeddedLdapServer.setPort(Integer.valueOf(ldapPort);
//        embeddedLdapServer.setPartitionSuffix(ldapBase);
//        return embeddedLdapServer;
//    }
//
//    @Bean
//    public TestContextSourceFactoryBean contextSource () {
//        TestContextSourceFactoryBean contextSource= new TestContextSourceFactoryBean();
//        contextSource.setPort(Integer.valueOf(ldapPort));
//        contextSource.setDefaultPartitionSuffix(ldapBase);
//        contextSource.setPrincipal(ldapUsername);
//        contextSource.setPassword(ldapPassword);
//        return contextSource;
//    }
//
//    @Bean
//    public LdifPopulator ldifPolulator() {
//        LdifPopulator ldifPopulator = new LdifPopulator();
//        ldifPopulator.setContextSource((ContextSource)contextSource());
//        ldifPopulator.setResource();
//        return ldifPopulator;
//    }
//
//    @Bean
//    public LdapTemplate ldapTemplate() {
//        return new LdapTemplate(contextSource());
//    }

//    @Bean
//    public DataSource dataSource() {
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName(jdbcDriverclassname);
//        dataSource.setUrl(jdbcUrl);
//        dataSource.setUsername(jdbcUsername);
//        dataSource.setPassword(jdbcPassword);
//
//        return dataSource;
//    }
//
//    @Bean
//    public JdbcTemplate jdbcTemplate() {
//        return new JdbcTemplate(dataSource());
//    }
//
//    @Bean
//    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
//        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
//        return propertySourcesPlaceholderConfigurer;
//    }
}