package fr.fduermael.demobatchspring5;

import fr.fduermael.demobatchspring5.repository.UserLdapRepository;
import fr.fduermael.demobatchspring5.repository.UserLdapRepositoryImpl;


import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.data.ldap.DataLdapTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import org.junit.runner.RunWith;
import org.junit.jupiter.api.Test;

/*


Must put these annotations on test class context:

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { UserRepository.class, LdapTemplate.class })
@ContextConfiguration(classes = Config.class)
@EnableConfigurationProperties

@DataLdapTest()
 */

@RunWith(SpringRunner.class)
//@SpringBootTest(classes = {UserLdapRepositoryImpl.class,LdapTemplate.class}/*classes = { DemoBatchSpring5ApplicationTests.class,SpringLdapIntegrationTests.class}*/)
@SpringBootTest

@ContextConfiguration(classes = TestConfig.class)
//@EnableConfigurationProperties
//@SpringBootConfiguration
//@AutoConfigurationPackage

/*
@RunWith(FrameworkRunner.class)
@CreateDS(name = "myDS",
		partitions = {
				@CreatePartition(name = "test", suffix = "o=gouv,c=fr")
		})
@CreateLdapServer(transports = { @CreateTransport(protocol = "LDAP", address = "localhost")})
@ApplyLdifFiles({"test-schema.ldif"})
*/

class DemoBatchSpring5ApplicationTests  {


	@Autowired
	private UserLdapRepository userLdapRepository;



	@Test
	public void testGetAllUserNames() {
		List<String> usernames = userLdapRepository.getAllUserNames();
		assertNotNull(usernames);
		assertEquals(usernames.size(), 1);
	}



	@Test
	void contextLoads() {
	}

}
